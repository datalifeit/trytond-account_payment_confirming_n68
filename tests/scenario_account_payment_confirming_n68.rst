=======================================
Account Payment Confirming N68 Scenario
=======================================

Imports::

    >>> import os
    >>> import io
    >>> import datetime
    >>> from decimal import Decimal
    >>> from proteus import Model, Wizard
    >>> from trytond.tests.tools import activate_modules
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.account.tests.tools import create_fiscalyear, \
    ...     create_chart, get_accounts
    >>> from trytond.modules.account_invoice.tests.tools import \
    ...     set_fiscalyear_invoice_sequences
    >>> today = datetime.datetime.now()

Install account_payment_confirming_n68::

    >>> config = activate_modules('account_payment_confirming_n68')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Create fiscal year::

    >>> fiscalyear = set_fiscalyear_invoice_sequences(
    ...     create_fiscalyear(company, today=datetime.datetime(2018, 7, 1)))
    >>> fiscalyear.click('create_period')

Create chart of accounts::

    >>> _ = create_chart(company)
    >>> accounts = get_accounts(company)
    >>> payable = accounts['payable']
    >>> receivable = accounts['receivable']
    >>> expense = accounts['expense']
    >>> Journal = Model.get('account.journal')

Add bank to company::

    >>> Party = Model.get('party.party')
    >>> Bank = Model.get('bank')
    >>> BankAccount = Model.get('bank.account')
    >>> party = Party(name='Bank')
    >>> party.save()
    >>> bank = Bank(party=party)
    >>> bank.save()
    >>> bank_account = BankAccount(bank=bank)
    >>> bank_account.owners.append(company.party)
    >>> iban_company = bank_account.numbers.new()
    >>> iban_company.type = 'iban'
    >>> iban_company.number = 'BE82068896274468'
    >>> bank_account.save()
    >>> iban_company = bank_account.numbers[0]

Create payment journal::

    >>> PaymentJournal = Model.get('account.payment.journal')
    >>> PaymentType = Model.get('account.payment.type')
    >>> ptype = PaymentType(kind='payable', name='Type 1')
    >>> ptype.save()
    >>> payment_journal = PaymentJournal(name='Confirming',
    ...     process_method='confirming')
    >>> payment_journal.party = company.party
    >>> payment_journal.payment_type = ptype
    >>> payment_journal.confirming_bank_account_number = iban_company
    >>> payment_journal.confirming_payable_flavor = 'n68'
    >>> payment_journal.save()

Create parties::

    >>> party = Party(name='Party 1')
    >>> party.save()
    >>> bank_account = BankAccount(bank=bank)
    >>> bank_account.owners.append(party)
    >>> iban = bank_account.numbers.new()
    >>> iban.type = 'iban'
    >>> iban.number = 'DE12500105170648489890'
    >>> bank_account.save()
    >>> party = Party(party.id)
    >>> party2 = Party(name='John Smith Jackson')
    >>> party2.save()
    >>> bank_account = BankAccount(bank=bank)
    >>> bank_account.owners.append(party2)
    >>> iban = bank_account.numbers.new()
    >>> iban.type = 'iban'
    >>> iban.number = 'ES7921000813610123456789'
    >>> bank_account.save()
    >>> party2 = Party(party2.id)

Create invoices::

    >>> PaymentTerm = Model.get('account.invoice.payment_term')
    >>> term = PaymentTerm(name='10D')
    >>> term_line = term.lines.new()
    >>> term_line.type = 'remainder'
    >>> term_line_delta = term_line.relativedeltas.new()
    >>> term_line_delta.days = 10
    >>> term.save()
    >>> Invoice = Model.get('account.invoice')
    >>> invoice = Invoice(type='in')
    >>> invoice.party = party
    >>> invoice.reference = 'N17'
    >>> invoice.payment_term = term
    >>> invoice.invoice_date = datetime.datetime(2018, 6, 18)
    >>> line = invoice.lines.new()
    >>> line.description = 'Description'
    >>> line.account = expense
    >>> line.quantity = 1
    >>> line.unit_price = Decimal('100')
    >>> invoice.click('post')
    >>> line_to_pay, = invoice.lines_to_pay
    >>> invoice2 = Invoice(type='in')
    >>> invoice2.party = party2
    >>> invoice.payment_term = term
    >>> invoice2.reference = 'R-5'
    >>> invoice2.invoice_date = datetime.datetime(2018, 5, 25)
    >>> line = invoice2.lines.new()
    >>> line.description = 'Description'
    >>> line.account = expense
    >>> line.quantity = 1
    >>> line.unit_price = Decimal('180')
    >>> invoice2.click('post')
    >>> line_to_pay2, = invoice2.lines_to_pay

Pay invoices::

    >>> Payment = Model.get('account.payment')
    >>> pay_line = Wizard('account.move.line.pay', [line_to_pay, line_to_pay2])
    >>> pay_line.form.date = today
    >>> pay_line.execute('next_')
    >>> pay_line.form.journal = payment_journal
    >>> pay_line.execute('next_')
    >>> payments = Payment.find()
    >>> for payment in payments:
    ...     payment.click('submit')
    ...     payment.click('approve')
    >>> len(payments)
    2

Process the payment::

    >>> PaymentGroup = Model.get('account.payment.group')
    >>> with config.set_context(active_ids=[p.id for p in payments]):
    ...     process_payment = Wizard('account.payment.process', payments)
    >>> party2.save()
    >>> process_payment.execute('process')
    >>> group, = PaymentGroup.find()
    >>> message, = group.confirming_messages
    >>> file = os.path.join(os.path.dirname(__file__), 'sample.txt')
    >>> sample = io.open(file, 'rb').read().decode('ascii')
    >>> sample = (sample % {
    ... 'today': today.strftime('%d%m%y'),
    ... 'date_year': today.strftime('%y%d%m%Y'),
    ... 'date': today.strftime('%d%m%Y')
    ... })
    >>> sample == message.message
    True
